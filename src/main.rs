/*
    Copyright (C) 2022 John Goerzen <jgoerzen@complete.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use filespooler::cmd;
use filespooler::cmd::cmd_exec::*;
use filespooler::cmd::cmd_info::*;
use filespooler::cmd::cmd_prepare::*;
use filespooler::cmd::cmd_queue::*;
use filespooler::logging::init_tracing;
use tracing::*;

fn main() -> Result<(), anyhow::Error> {
    let cliopts = cmd::parse();
    init_tracing(cliopts.globalopts.log_level);
    trace!("Parsed options are {:?}", cliopts);
    match cliopts.command {
        cmd::Command::Prepare(po) => cmd_prepare(&po),
        cmd::Command::PrepareNOP(po) => cmd_preparenop(&po),
        cmd::Command::PrepareFail(po) => cmd_preparefail(&po),
        cmd::Command::PrepareGetNext(po) => cmd_preparegetnext(&po),
        cmd::Command::PrepareSetNext(po) => cmd_preparesetnext(&po),
        cmd::Command::StdinInfo => cmd_infostdin(),
        cmd::Command::StdinPayload => cmd_payloadstdin(),
        cmd::Command::QueueInit(qopts) => cmd_queueinit(&qopts),
        cmd::Command::QueueWrite(qopts) => cmd_queuewrite(&qopts),
        cmd::Command::QueueLs(qopts) => cmd_queuels(&qopts),
        cmd::Command::QueueInfo(qopts) => cmd_queueinfo(&qopts),
        cmd::Command::QueuePayload(qopts) => cmd_queuepayload(&qopts),
        cmd::Command::StdinProcess(opts) => cmd_execstdin(&opts),
        cmd::Command::QueueProcess(opts) => cmd_execqueue(&opts),
        cmd::Command::QueueGetNext(opts) => cmd_queuegetnext(&opts),
        cmd::Command::QueueSetNext(opts) => cmd_queuesetnext(&opts),
        cmd::Command::ShowLicense => cmd::cmd_showlicense(),
        cmd::Command::GenUUID => cmd::cmd_genuuid(),
        cmd::Command::GenFilename => cmd::cmd_genfilename(),
    }
}

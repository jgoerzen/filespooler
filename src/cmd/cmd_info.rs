/*
    Copyright (C) 2022 John Goerzen <jgoerzen@complete.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

use crate::jobfile;

/// Displays info about a packet piped in on stdin
pub fn cmd_infostdin() -> Result<(), anyhow::Error> {
    let stdin_base = std::io::stdin();
    let mut stdin_fd = stdin_base.lock();

    let meta = jobfile::read_jobfile_header(&mut stdin_fd)?;
    meta.render(None, None, &mut std::io::stdout())?;
    Ok(())
}

/// Dumps the payload from a packet piped in on stdin
pub fn cmd_payloadstdin() -> Result<(), anyhow::Error> {
    let stdin_base = std::io::stdin();
    let mut stdin_fd = stdin_base.lock();

    jobfile::read_jobfile_header(&mut stdin_fd)?;
    std::io::copy(&mut stdin_fd, &mut std::io::stdout())?;
    Ok(())
}
